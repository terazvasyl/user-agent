package com.example.docker;

import lombok.extern.log4j.Log4j;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Log4j
@RestController
public class IndexController {

    @RequestMapping("/")
    public String getUserAgent(@RequestHeader("User-Agent") String userAgent) {
        int a = 1000;
        log.info("Accepted request from - " + userAgent);
        return userAgent;
    }
}
